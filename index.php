<?php

$dbname='base';
$mytable ="seedstars";

if(!class_exists('SQLite3'))
  die("SQLite 3 NOT supported.");
  
$base=new SQLite3($dbname, 0666); 

$query = "CREATE TABLE $mytable(
            name varchar(200) NOT NULL,            
            color varchar(200) NOT NULL         
            )";
            
$results = $base->exec($query);
if (!$results)
    exit ("Table not created<br>");

echo "Table '$mytable' created.";
echo '<br/>';
//fetch data from jenkins
$results = file_get_contents('http://solomond6:temisolo17@localhost:8080/api/json');
$jobs = json_decode($results)->jobs;
echo '<table border=1>';
echo '<thead><th>Name</th><th>Color</th></thead>';
foreach ($jobs as $value){
		echo '<tr><td>'.$value->name.'</td><td>'.$value->color.'</td></tr>';
}
echo '</table>';

//inserting fetched data into sqlite
foreach ($jobs as $value){
		// echo '<tr><td>'.$value->name.'</td><td>'.$value->color.'</td></tr>';
		$query1 = "INSERT INTO $mytable(name, color) VALUES ('$value->name', '$value->color')";
		$results1 = $base->exec($query1);
}

if(!$results1)
  die("<i>$mytable</i> not updated.");

echo '<br/>';

echo "Data entered into <i>$mytable</i> successfully.";

echo '<br/>';

$query2 = "SELECT name, color FROM $mytable";
$results2 = $base->query($query2);

$row = $results2->fetchArray();

var_dump($row);

if(count($row)>0)
{
   $name = $row['name'];
   $color = $row['color']; 
     
   echo "<p>$name</p>\n";
   echo "<p>$color</p>\n";
}
else
{
  echo "Can't access $mytable table.";
}

?>